HydrOffice StormFix
===================

.. image:: https://github.com/hydroffice/hyo_stormfix/raw/master/py/media/stormfix.png
    :alt: logo


* Code: `GitHub repo <https://github.com/hydroffice/hyo_stormfix>`_
* Project page: `url <https://www.hydroffice.org/stormfix/main>`_
* Download page: `url <https://bitbucket.org/hydroffice/hyo_stormfix/downloads/>`_
* License: LGPLv3 (See `LICENSE <https://www.hydroffice.org/license/>`_)

|

General Info
------------

.. image:: https://api.codacy.com/project/badge/Grade/759aa2b185434305b0a3acfd6d2261eb
    :target: https://www.codacy.com/app/hydroffice/hyo_stormfix
    :alt: Codacy


HydrOffice is a research development environment for ocean mapping. It provides a collection of hydro-packages,
each of them dealing with a specific issue of the field.
The main goal is to speed up both algorithms testing and research-to-operation (R2O).

HydrOffice Stormfix is a collections of tools to reduce artifacts in acoustic backscatter data.
